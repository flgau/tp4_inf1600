.globl	_ZNK9Rectangle11DiagonalAsmEv

_ZNK9Rectangle11DiagonalAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
        mov 8(%ebp), %eax
        sub $4, %esp #(au début)


        fld 4(%eax) #on met length dans la pile st[0] = length
        fld 4(%eax) #on met length dans la pile st[1] = length et st[0] = length
        fmulp       # st[0] = length* length, st[1] = length* length
        fstps -4(%ebp) #st[0] est transfere dans (%ebx) et st[1] devient st[0] donc st[0] = length* length*/
        fld 8(%eax) #on met width dans st[0], st[1] = length*length
        fld 8(%eax) #on met width dans st[0], st[1] = width
        fmulp       # st[0] = width* width, st[1] = width* width
        flds -4(%ebp)  # st[0] = length*length, st[1] = width*width
        faddp       # st[0] = length*length + width*width, st[1] = length*length + width*width 
        fsqrt       # st[0] = sqrt(length*length + width*width)*/




        
        add $4, %esp # (à la fin)
        leave          /* restore ebp and esp */
        ret            /* return to the caller */
