.globl	_ZNK9Rectangle12PerimeterAsmEv

factor: .float 2.0 /* use this to mult by two */

_ZNK9Rectangle12PerimeterAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
        
        movl 8(%ebp), %eax
        fld 4(%eax) /* ajouter a la pile de float la valeur de length */
        fld 8(%eax) /* ajouter a la pile de float la valeur de width */

        faddp        /* length + width */
        fld factor   /* ajoute 2 a la pile float */
        fmulp        /* (length + width) * 2 */
         

        leave          /* restore ebp and esp */
        ret            /* return to the caller */
