.globl	_ZNK8Cylinder12PerimeterAsmEv

factor: .float 2.0 /* use this to mult by two */

_ZNK8Cylinder12PerimeterAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
        movl 8(%ebp), %eax
        fld 4(%eax) /* ajouter a la pile de float la valeur de radius */
        fldpi
        fmulp        /* radius *pi */
        fld factor   /* ajoute 2 a la pile float */
        fmulp        /* radius *pi* 2 */
         

        
        leave          /* restore ebp and esp */
        ret            /* return to the caller */
