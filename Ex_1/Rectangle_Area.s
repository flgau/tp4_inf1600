.globl	_ZNK9Rectangle7AreaAsmEv

_ZNK9Rectangle7AreaAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
        movl 8(%ebp), %eax
        fld 4(%eax) /* ajouter a la pile de float la valeur de length */
        fld 8(%eax) /* ajouter a la pile de float la valeur de width */

        fmulp        /* (length + width) */

        leave          /* restore ebp and esp */
        ret            /* return to the caller */
