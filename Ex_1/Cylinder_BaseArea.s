.globl	_ZNK8Cylinder11BaseAreaAsmEv

_ZNK8Cylinder11BaseAreaAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */

         movl 8(%ebp), %eax
        fld 4(%eax) /* ajouter a la pile de float la valeur de radius */
        fldpi        /* ajouter a la pile de float la valeur de pi */
        fmulp        /* radius *pi */
        fld 4(%eax) /* ajouter a la pile de float la valeur de radius */
        fmulp        /* radius *pi* radius */
         
        
        leave          /* restore ebp and esp */
        ret            /* return to the caller */
