.globl	_ZNK8Cylinder14LateralAreaAsmEv

_ZNK8Cylinder14LateralAreaAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
        
        movl 8(%ebp), %eax
        mov 0(%eax), %ecx

        call *12(%ecx)   /* appeler Cylinder::PerimeterAsm */
        fld 8(%eax)      /* ajouter a la pile de float la valeur de height */
        faddp            /* perimetre + hauteur */

        leave          /* restore ebp and esp */
        ret            /* return to the caller */
